package nl.dionvandam.adapter;

import android.os.AsyncTask;

import org.thoughtcrime.securesms.logging.Log;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Scanner;

public class Adapter extends AsyncTask<Void, Void, Void>
{
    @Override
    protected Void doInBackground(Void... voids) {
        int         port        = 8888;
        int         state       = 0;
        Alphabet    response    = Alphabet.NULL;

        try {
            ServerSocket listener = new ServerSocket(port);
            Log.i("ADAPTER", "Server connected to");
            Log.i("ADAPTER", "IP address: " + getIpAddress());
            Log.i("ADAPTER", "port:       " + port);

            Socket socket = listener.accept();
            while (true) {
                try {
                    Scanner         in  = new Scanner(socket.getInputStream());
                    PrintWriter     out = new PrintWriter(socket.getOutputStream(), true);
                    while (in.hasNextLine()) {
                        switch (Alphabet.toEnum(in.nextLine())) {
                            case RESET:
                                state       = 0;
                                response    = Alphabet.DONE;
                                break;
                            case A:
                                if (state == 0) response = Alphabet.C;
                                else            response = Alphabet.D;
                                break;
                            case B:
                                state       = 1 - state;
                                response    = Alphabet.E;
                                break;
                        }
                        out.println(response);
                    }
                } catch (Exception e) {
                    Log.e("ADAPTER","Server error (" + socket + ")");
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.i("ADAPTER","Server closed (" + socket + ")");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // source: http://androidsrc.net/android-client-server-using-sockets-server-implementation/
    public String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress
                            .nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }
        return ip;
    }
}