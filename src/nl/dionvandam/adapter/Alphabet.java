package nl.dionvandam.adapter;

public enum Alphabet {
    RESET,
    NULL,

    // Input
    RECEIVE_MESSAGE_CORRECT,
    RECEIVE_MESSAGE_INVALID_MESSAGE,
    RECEIVE_MESSAGE_DUPLICATE_MESSAGE,
    RECEIVE_MESSAGE_LEGACY_MESSAGE,
    RECEIVE_MESSAGE_INVALID_KEY_ID,
    RECEIVE_MESSAGE_INVALID_KEY,
    RECEIVE_MESSAGE_UNTRUSTED_IDENTITY,

    // Output
    DECRYPT_MESSAGE,
    ERROR_INVALID_MESSAGE,
    ERROR_DUPLICATE_MESSAGE,
    ERROR_LEGACY_MESSAGE,
    ERROR_INVALID_KEY_ID,
    ERROR_INVALID_KEY,
    ERROR_UNTRUSTED_IDENTITY;

    public static Alphabet toEnum(String s) {
        try {
            return valueOf(s);
        } catch (IllegalArgumentException e) {
            return NULL;
        }
    }
}